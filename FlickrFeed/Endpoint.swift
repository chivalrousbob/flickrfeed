//
//  Endpoint.swift
//  FlickrFeed
//
//  Created by Ayoub NOURI on 22/10/2017.
//  Copyright © 2017 Ayoub NOURI. All rights reserved.
//

import Moya

enum Endpoint {
    case feed
}

extension Endpoint: TargetType {
    
    var baseURL: URL{
        return URL(string:"https://api.flickr.com")!
    }
    var path: String{
        switch self {
        case .feed:
            return "/services/feeds/photos_public.gne?format=json&nojsoncallback=1"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .feed:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .feed:
            return [:]
        }
    }
    
    var task: Task {
        switch self {
        case .feed:
            return .request
        }
    }
    
    var sampleData: Data {
        switch self {
        case .feed:
            return "{'sample':'data'}".utf8Encoded
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .feed:
            return URLEncoding.httpBody
        }
    }
    
}

