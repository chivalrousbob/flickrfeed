//
//  ViewController.swift
//  FlickrFeed
//
//  Created by Ayoub NOURI on 22/10/2017.
//  Copyright © 2017 Ayoub NOURI. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FeedCollectionViewController: UICollectionViewController {
    
    // MARK: - Properties
    
    let viewModel = PhotoViewModel()
    var disposeBag : DisposeBag!

    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setupAspect()
        
        setupRX()
        
    }
    
    // MARK: - RX
    
    func setupRX(){
        self.disposeBag = DisposeBag()
        self.collectionView?.delegate = nil
        self.collectionView?.dataSource = nil
        viewModel.photos.asObservable().bind(to: self.collectionView!
            .rx
            .items(cellIdentifier: PhotoCollectionViewCell.reuseIdentifier,cellType: PhotoCollectionViewCell.self)){
                row,photo,cell in
                cell.configure(photo: photo)
            }.addDisposableTo(disposeBag)
        
        collectionView!.rx.setDelegate(self)
            .addDisposableTo(disposeBag)
    }
    
}



