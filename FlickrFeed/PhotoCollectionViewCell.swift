//
//  PhotoTableViewCell.swift
//  FlickrFeed
//
//  Created by Ayoub NOURI on 22/10/2017.
//  Copyright © 2017 Ayoub NOURI. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoCollectionViewCell: UICollectionViewCell {
    
    // MARK: - @IBOutlet
    
    @IBOutlet weak var imageViewPhoto: UIImageView!
    
    
    // MARK: - Properties
    
    class var reuseIdentifier: String {
        return "photoCell"
    }
    
    
    // MARK: - Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    // MARK: - Action
    
    func configure(photo: Photo){
        if let image = photo.image {
            self.imageViewPhoto.sd_setImageWithPreviousCachedImage(with: URL(string: image),placeholderImage: UIImage(named: "placeholder"), progress: nil, completed: nil)
        }
        
    }
    
}


