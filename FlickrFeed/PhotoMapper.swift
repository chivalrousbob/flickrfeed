//
//  PhotoMapper.swift
//  FlickrFeed
//
//  Created by Ayoub NOURI on 22/10/2017.
//  Copyright © 2017 Ayoub NOURI. All rights reserved.
//

import Moya_SwiftyJSONMapper
import SwiftyJSON

final class PhotoMapper: NSObject, ALSwiftyJSONAble {
    
    // MARK: - Properties
    
    var photos = [Photo]()
    
    
    // MARK: - Init
    
    required init?(jsonData: JSON) {
        super.init()
        
        if let items = jsonData["items"].array{
            
            for item in items{
                var photo = Photo()
                
                if let media = item["media"].dictionary{
                    
                    if let image = media["m"]?.string{
                        photo.image = image
                        self.photos.append(photo)
                    }
                    
                }
            }
        }
        
    }
}
