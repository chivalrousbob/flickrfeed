//
//  PhotoViewModel.swift
//  FlickrFeed
//
//  Created by Ayoub NOURI on 22/10/2017.
//  Copyright © 2017 Ayoub NOURI. All rights reserved.
//

import RxSwift
import Moya

struct PhotoViewModel {
    
    // MARK: - Properties
    
    let photos = Variable<[Photo]>([])
    var error: Variable<NSError?> = Variable(nil)
    let disposeBag = DisposeBag()
    
    
    // MARK: - Init
    
    init() {
        self.getPublicFeed()
    }
    
    
    // MARK: - RX
    
    private func getPublicFeed() {
        let provider = RxMoyaProvider<Endpoint>()
        
        provider.request(.feed).subscribe{ event in
            switch event{
            case .next(let response):
                do{
                    let item = try response.map(to: PhotoMapper.self)
                    self.photos.value = item.photos
                }catch(let err){
                    
                    print("Invalid Json format \(err)")
                    //sometimes the json recieved from Flickr API is Broken so i add this line in order to resend my request
                    self.getPublicFeed()
                }
            case .error(let error):
                self.error.value = error as NSError
                print("error  \(error)")
            default:
                break
            }
            }.addDisposableTo(disposeBag)
    }
    
}

