//
//  String.swift
//  FlickrFeed
//
//  Created by Ayoub NOURI on 22/10/2017.
//  Copyright © 2017 Ayoub NOURI. All rights reserved.
//

import Foundation

extension  String{
    
    // MARK: - Properties
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
    
}
