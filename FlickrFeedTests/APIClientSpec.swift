
//
//  APIClientSpec.swift
//  FlickrFeed
//
//  Created by Ayoub NOURI on 22/10/2017.
//  Copyright © 2017 Ayoub NOURI. All rights reserved.
//

import Quick
import Nimble
import Mockingjay
import RxSwift
@testable import FlickrFeed

class APIClientSpec: QuickSpec {
    
    override func spec(){
        super.spec()
        
        
        
        describe("requestFeed"){
            
            context("success"){
                it("returns Publuc feed "){
                    let testBundle = Bundle(for: type(of: self))
                    if let path = testBundle.path(forResource: "GetFeedSuccess", ofType: "json"){
                        do{
                            let data = try Data(contentsOf: URL(fileURLWithPath: path))
                            
                            self.stub(uri("https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1"), jsonData(data))
                            let viewModel = PhotoViewModel()
                            
                            
                            expect(viewModel.photos.value).toEventuallyNot(beEmpty(),timeout: 3)
                            expect(viewModel.photos.value[0].image) == "https://farm5.staticflickr.com/4477/24008596528_42b9a85ae5_m.jpg"
                            
                        }catch(let error){
                            print("cannot read the json file content into Data Object  \(error)")
                        }
                    }
                    
                }
            }
            context("error"){
                it("returns error"){
                    let error = NSError(domain: "link not found", code: 404, userInfo: nil)
                    self.stub(uri("https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1"), failure(error))
                    let viewModel = PhotoViewModel()
                    expect(viewModel.error).toEventuallyNot(beNil())
                }
            }
            
        }
    }
}
