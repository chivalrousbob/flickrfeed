//
//  Photo.swift
//  FlickrFeed
//
//  Created by Ayoub NOURI on 22/10/2017.
//  Copyright © 2017 Ayoub NOURI. All rights reserved.
//

struct Photo {
    
    // MARK: - Properties
    
    var image: String?
    
}
